#1/bin/bash

##############################################################################
################################  VARIABLES  #################################
##############################################################################

GH_TOKEN=$1
REPO=$2
BRANCH=$3

##############################################################################
###############################  ACTUAL SCRIPT ###############################
##############################################################################

service docker start
service docker status

git config --global user.name 'saurajbot'
git config --global user.email 'rommirrorer@gmail.com'


git clone https://${GH_TOKEN}@github.com/${REPO} source
cd source || exit 1
ls
docker container prune --force || true
docker build . --rm --force-rm --compress --no-cache=true --pull --file Dockerfile -t mirror-bot
docker image ls
docker run --privileged --rm -i mirror-bot

###############################################################################
#################################### END ######################################
###############################################################################
